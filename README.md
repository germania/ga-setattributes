#ga-setattributes

Node/CommonJS/AMD module and global function for setting attribute values on DOM elements.

##Installation

```bash
npm install ga-setattributes --save
```

##Usage

The script checks for each attribute if it is in the element already, i.e. if it is native. Otherwise *element.setAttribute* will be used – except for *style* and *html* which get a special treatment.


```js
// Require
var setAttributes = require('ga-setattributes');

// Pass element and attributes literal
setAttributes(targetObject, {
	class: "foo bar",
	alt: "An image"
});
```

###Removing attributes
Just pass *null* on an attribute you want to remove. Internally, `element.removeAttribute` will be used.

```js
setAttributes(targetObject, {
	class: null
});
```

###Styles and HTML

```js
setAttributes(targetObject, {
	html: "Click me",
	style: {
		color: "red"
	}
});
```

##Develop with Gulp

Use [Git Flow](https://github.com/nvie/gitflow), always work in `develop` branch.

- Install development dependencies: `npm install`
- Run `gulp watch`
- Work in `src/`

Builds are now in `dist/`