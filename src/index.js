(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === "object") {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory( );
    } else {
        // Browser globals (root is window)
        root.setAttributes = factory();
    }
}(this, function () {

    function setAttributes ( element, attributes ) {

        Object.getOwnPropertyNames( attributes ).forEach(function( name ) {

            if ((name === "styles" || name === "style") && typeof attributes[name] === "object") {
                Object.getOwnPropertyNames( attributes[name] ).forEach(function( prop ) {
                    element.style[prop] = attributes[name][prop];
                });
            }
            else if (name === "html") {
                element.innerHTML = attributes[name];
            }
            else if (attributes[name] === null) {
                element.removeAttribute( name );
            }
            else if (name in element) {
                element[ name ] = attributes[name];
            }
            else {
                element.setAttribute( name , attributes[name] );
            }
        });
    }

    return setAttributes;
}));
